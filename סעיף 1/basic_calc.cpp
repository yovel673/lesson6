#include <iostream>
int add(int a, int b, bool &valid) {
	if ((a + b) == 8200)
	{
		std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
		valid = false;
	}
	return a + b;
}

int  multiply(int a, int b, bool &valid) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, valid);
	if (valid == false) 
	{
		return -1;
	}

  };

  return sum;
}

int  pow(int a, int b, bool& valid) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a, valid);
  };
  return exponent;
}

void print(int result, bool& valid)
{
	if (valid)
	{
		std::cout << result << std::endl;
	}
	valid = true; //reseting valid
}


int main(void) {
  
	bool valid = true;
	int result = 0;
	//checking for add
	result = add(4100, 4100, valid); //invalid
	print(result, valid);

	result = add(5642, 1651, valid); //valid
	print(result, valid);

	//checking for multiply
	result = multiply(100, 100, valid); //invalid
	print(result, valid);

	result = multiply(500, 20, valid); //valid
	print(result, valid);


	//checking for pow
	result = pow(100, 10, valid); //invalid
	print(result, valid);

	result = pow(2, 20, valid); //valid
	print(result, valid);





  system("pause");
  return 0;
}