#pragma once
#include <exception>
class InputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "The given Input is invalid!!\n";
	}
};