#pragma once
#include "shape.h"

class Pentagon : public Shape
{
public:
	Pentagon(std::string nam, std::string col, double len);
	void setLength(double len);
	void draw();
private:
	double length;

};
