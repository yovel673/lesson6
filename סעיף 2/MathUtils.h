#pragma once
class MathUtils
{
public:
	static double CalPentagonArea(double length);
	static double CalHexagonArea(double length);
};