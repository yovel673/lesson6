#include "Hexagon.h"
#include "MathUtils.h"

Hexagon::Hexagon(std::string nam, std::string col, double len) : Shape(nam, col)
{
	length = len;
}


void Hexagon::setLength(double len)
{
	length = len;
}


void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "length is " << length << std::endl << "area is " << MathUtils::CalHexagonArea(length) << std::endl;
}
