#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include <string.h>
#include "shapeException.h"
#include "InputException.h"
#include "Hexagon.h"
#include "Pentagon.h"


int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, len = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(nam, col, len);
	Pentagon pen(nam, col, len);


	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;

	Shape* ptrhex = &hex;
	Shape* ptrpen = &pen;

	std::string shapename;

	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', hexagon = 'h', pentagon = 'P'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, P = pentagon" << std::endl;
		std::cin >> shapename;
		rad = 0, ang = 0, ang2 = 0, len = 0; //reseting fields for next shape
		height = 0, width = 0;

		if (shapename.length() > 1)
		{
			std::cout << "Warning - Don't try to build more than one shape at once\n" << std::endl;
		}
		shapetype = shapename[0]; //taking only the first letter
try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				if (rad == NULL)
				{
					throw InputException();
				}
				else if (rad < 0)
				{
					throw shapeException();
				}

				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (height == NULL || width == NULL)
				{
					throw InputException();
				}
				else if (height < 0 || width < 0)
				{
					throw shapeException();
				}

				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (width == NULL || height == NULL)
				{
					throw InputException();
				}
				else if (height < 0 || width < 0)
				{
					throw shapeException();
				}

				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (height == NULL || width == NULL || (ang == NULL && ang2 == NULL))
				{                                     
					throw InputException();
				}
				else if (height <= 0 || width <= 0 || ang + ang2 != 180 || ang < 0 || ang2 < 0)
				{
					throw shapeException();
				}

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			case 'P':
				std::cout << "enter name, color, len" << std::endl;
				std::cin >> nam >> col >> len;

				if (len == NULL)
				{
					throw InputException();
				}
				else if (len < 0)
				{
					throw shapeException();
				}
				pen.setName(nam);
				pen.setColor(col);
				pen.setLength(len);
				ptrpen->draw(); 
				break;
			case 'h':
				std::cout << "enter name, color, len" << std::endl;
				std::cin >> nam >> col >> len;

				if (len == NULL)
				{
					throw InputException();
				}
				else if (len < 0)
				{
					throw shapeException();
				}
				hex.setName(nam);
				hex.setColor(col);
				hex.setLength(len);
				ptrhex->draw();
				break;


			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (const InputException& e)
		{
			printf(e.what());
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		catch (std::exception& e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}