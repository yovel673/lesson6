#pragma once
#include "shape.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string nam, std::string col, double len);
	void setLength(double len);
	void draw();

private:
	double length;

};
