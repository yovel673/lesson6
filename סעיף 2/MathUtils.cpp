#include "MathUtils.h"


double MathUtils::CalPentagonArea(double length)
{
	return (5 * length*length* 1.37638192047) / 4;
}

double MathUtils::CalHexagonArea(double length)
{
	return 2.59807* length * length;
}